import {Injectable} from '@angular/core';  
import { PayloadAction as Action } from '../actions/action';

import { JobPackage } from '../models/package';

@Injectable()
export class PackageActions {

    static ADD_PACKAGE = 'ADD_PACKAGE';
    addPackage(packageList: JobPackage): Action {
        return {
            type: PackageActions.ADD_PACKAGE,
            payload: packageList
        }
    }

    static TRACK_PACKAGE = 'TRACK_PACKAGE'
    trackPackage(trackingNumber): Action {
        return {
            type: PackageActions.TRACK_PACKAGE,
            // payload: trackingNumber
        }
    }

}