export class JobPackage {
    _id: string
    tracking_number: number
    email: string
    senders_name: string
    pickup_location: string
    senders_phone: string
    receiver_name: string
    receivers_destination_address: string
    contact_number_person: string
    item_description: string
    pickup_time: Date
    special_request: string       
    constructor () {
        this._id = new Date().toISOString()
    }
}