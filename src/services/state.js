import { JobPackage } from '../models/package'

export interface AppState {
    jobpackages: JobPackage[]
}
