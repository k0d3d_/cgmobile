import {ActionReducer, Action} from '@ngrx/store';  
import { PackageActions } from '../actions/package-action';

let nextId = 0;

export function PackageReducer (state = [], action) {
    switch (action.type) {
        case PackageActions.ADD_PACKAGE:
            return [...state, Object.assign({}, action.payload, { id: nextId++ })];
        // case PackageActions.UPDATE_BIRTHDAY:
        //     return state.map(birthday => {
        //         return birthday.id === action.payload.id ? Object.assign({}, birthday, action.payload) : birthday;
        //     });
        // case PackageActions.DELETE_BIRTHDAY:
        //     return state.filter(birthday => birthday.id !== action.payload.id);
        default:
            return state;        
    }
}
