import {
  Injectable
} from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';

import {
  JobPackage
} from '../../models/package'

import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';

declare var pouchdb: any;

/*
  Generated class for the WpApiServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
  LUQq wclL 4rq8 Fi5Z kQLU kmoH
*/
@Injectable()
export class WpApiServiceProvider {

  private _db;
  private _packages: JobPackage;
  private _PouchDb

  public serverUrl

  constructor(private http: HttpClient) {
    this._PouchDb = PouchDB
    this.serverUrl = 'https://comptongreen.com.ng/wp-json'
    PouchDB.plugin(cordovaSqlitePlugin);
    this._db = new PouchDB('jobpackage.db', {
      adapter: 'websql'
    });
  }


  db() {
    return this._db
  }

  getToken() {
    return btoa("apps:LUQq wclL 4rq8 Fi5Z kQLU kmoH")
  }

  requestNewPackage(packageData) {
    const headers = new HttpHeaders()
      .set("Authorization", `Basic ${this.getToken()}`);

    return this.http.post(`${this.serverUrl}/wp/v2/package`,
      packageData, {
        headers
      })
  }

  trackPackage(trackingNo) {
    const headers = new HttpHeaders()
      .set("Authorization", `Basic ${this.getToken()}`);

    return this.http.get(`${this.serverUrl}/cge/v1/track/${trackingNo}`, {
      headers      
    })
  }

}
