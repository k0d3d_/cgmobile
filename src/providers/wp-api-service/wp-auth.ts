
import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor} 
     from "@angular/common/http";
import {HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    
    constructor() {
    }

    getToken () {
      return btoa("apps:LUQq wclL 4rq8 Fi5Z kQLU kmoH")
    }    

    intercept(req: HttpRequest<any>, 
               next: HttpHandler):Observable<HttpEvent<any>> {
        
        // const clonedRequest = req.clone({
        //     setHeaders: {
        //         Authorization: `Basic ${this.getToken()}`
        //     }
        // });
        
        const clonedRequest = req.clone({
            headers: req.headers.set(
                'Authorization', `Basic ${this.getToken()}`
            )
        });
      
        console.log("new headers", clonedRequest.headers.keys());
        return next.handle(clonedRequest);
    }
}



      