import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { WpApiServiceProvider } from '../../providers/wp-api-service/wp-api-service'

import { HistoryPage } from '../history/history'
import { JobPackage } from '../../models/package'


/**
 * Generated class for the RequestPickupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-request-pickup',
  templateUrl: 'request-pickup.html',
})
export class RequestPickupPage {
  private formRequest: any
  protected loadingDialog: any
  protected jobpackage: JobPackage
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    protected wpService: WpApiServiceProvider
    ) {
      this.formRequest = {}
  }

  createRequest () {
    this.presentLoadingCustom()
    this.formRequest.status = 'pending'
    this.wpService.requestNewPackage(this.formRequest)
    .subscribe(data => {
      //save the response as a item in 
      // db, 
      // dismiss the dialog
      //toast and navigate
      // let packageI = 
      this.jobpackage = new JobPackage()
      let fieldList = [
        'email',
        'senders_name',
        'pickup_location',
        'senders_phone',
        'receiver_name',
        'receivers_destination_address',
        'contact_number_person',
        'item_description',
        'pickup_time',
        'special_request',  

      ]
      fieldList.forEach(v => {
        this.jobpackage[v] = data[v]
      })

      this.wpService.db().post(this.jobpackage)
      .then(() => {
        this.loadingDialog.dismiss()
      })
    })
  }
  presentLoadingCustom() {
    this.loadingDialog = this.loadingCtrl.create({
      spinner: 'crescent',
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box">
            <p>Please wait while your request is submitted.</p>
          </div>
        </div>`,
    });

    this.loadingDialog.onDidDismiss(() => {
      this.navCtrl.push(HistoryPage)
    });

    this.loadingDialog.present();
  }
}
