import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

declare let startSocialFeed:any
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController) {
  }
  ionViewDidLoad() {
    startSocialFeed()
    // Put here the code you want to execute
  }
}
