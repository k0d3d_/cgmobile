import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { WpApiServiceProvider } from '../../providers/wp-api-service/wp-api-service'
import { JobPackage } from '../../models/package'
// import { GreenBall } from './green-ball'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public trackingNo: string
  public package: any

  constructor(
    public navCtrl: NavController,
    public wpService: WpApiServiceProvider) {
    this.package = {}
  }

  trackPackage () {
    this.wpService.trackPackage(this.trackingNo)
    .subscribe(data => {
      this.package = data
    })
  }

}
