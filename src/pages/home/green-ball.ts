import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'green-ball',
  templateUrl: 'green-ball.html'
})
export class GreenBall {

  constructor(public navCtrl: NavController) {

  }

}
