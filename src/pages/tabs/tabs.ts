import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { HistoryPage } from '../history/history'
import { RequestPickupPage } from '../request-pickup/request-pickup'


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  HomePageRoot = HomePage
  AboutPageRoot = AboutPage
  ContactPageRoot = ContactPage
  HistoryPageRoot = HistoryPage
  RequestPickupPageRoot = RequestPickupPage

  constructor() {

  }
}
