import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RequestPickupPage } from '../request-pickup/request-pickup'
import { WpApiServiceProvider } from '../../providers/wp-api-service/wp-api-service'
import { JobPackage } from '../../models/package'

/**
 * Generated class for the HistoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {

  protected packages: any[]

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    protected wpService: WpApiServiceProvider) {
      this.packages = []
      this.packages.push({
        tracking_number: 1000001
      })
  }

  ionViewDidLoad() {
    this.wpService.db()
    .allDocs({
      include_docs: true,
      descending: true 
    })
    .then((listOfPackages) => {
      let c = this
      listOfPackages.rows.map(row => row.doc)
      .forEach(element => {
        c.packages.push(element)
      });
      console.log(this.packages)
    })
  }

  gotoPickUp () {
    this.navCtrl.push(RequestPickupPage)
  }

}
