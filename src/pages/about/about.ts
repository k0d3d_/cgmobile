import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  slides = [
    {
      title: "",
      description: "Provide very agile ‘door to door’ collection/delivery (mail/parcels) services to all destinations.",
      image: "./assets/images/maps-and-flags.png",
      imageAnt: "./assets/images/delivery-truck-2.png",
    },
    {
      title: "",
      description: "Provide a total support network that will satisfy all your Distribution and Logistic needs.",
      image: "./assets/images/boss.png",
      imageAnt: "./assets/images/delivery-truck-3.png"
    },
    {
      title: "",
      description: "Provide a seamless service to you and your customers.",
      image: "./assets/images/call-center.png",
      imageAnt: "./assets/images/phone-call.png"
    }
  ];
  constructor(public navCtrl: NavController) {

  }

}
