module.exports = {
  copyAppDotJs: {
    src: ['{{SRC}}/app.js'],
    dest: '{{BUILD}}'
  },
  copyTemplateJS: {
    src: '{{SRC}}/template.html',
    dest: '{{WWW}}'
  },
  copyAwesomeFonts: {
    src: '{{ROOT}}/libs/font-awesome/fonts/**/*',
    dest: '{{WWW}}/fonts'
  },
  copySocialFeed: {
    src: [
      '{{ROOT}}/libs/social-feed/css/jquery.socialfeed.css',
      '{{ROOT}}/libs/font-awesome/css/font-awesome.css',
      '{{ROOT}}/libs/jquery/dist/jquery.min.js',
      '{{ROOT}}/libs/codebird-js/codebird.js',
      '{{ROOT}}/libs/doT/doT.min.js',
      '{{ROOT}}/libs/moment/min/moment.min.js',
      '{{ROOT}}/libs/moment/locale/en-gb.js',
      '{{ROOT}}/libs/social-feed/js/jquery.socialfeed.js'
    ],
    dest: '{{WWW}}/vendors/'
  }
}
